const state = {
    chartTimeLog:'test'
  }
const mutations= {
    SET_CHART_TIME_LOG: (state, chartTimeLog) => {
        state.chartTimeLog = chartTimeLog
      },
}
const actions = {
    getChartTimeLog({ commit }, chartTimeLog){
        commit('SET_CHARTTIMELOG',chartTimeLog)
    }
}
export default{
    namespaced: true,
    state,
    mutations,
    actions
}