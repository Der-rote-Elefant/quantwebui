const state = {
    tableCodes: [
        { code: "rb999.FU" },
        { code: "j999.FU" },
        { code: "ag999.FU" },
        { code: "au999.FU" },
        { code: "MA999.FU" },
        { code: "i999.FU" },
        { code: "cs999.FU" },
        { code: "ZC999.FU" },
        { code: "ni999.FU" },
        { code: "fu999.FU" }
    ],
    tableIPList: [
        { ip: "localhost", port: 10000 },
        { ip: "localhost", port: 9999 }
    ],
    showChart: []
}
const mutations = {
    SET_TABLE_CODE: (state, tableCodes) => {
        state.tableCodes = tableCodes
    },
    SET_IP_LIST: (state, tableIPList) => {
        state.tableIPList = tableIPList
    },
    SET_SHOW_Chart: (state, showChart) => {
        state.showChart = showChart
    }
}
const actions = {
    getTableCodes({ commit }, chartTimeLog) {
        commit('SET_TABLE_CODE', chartTimeLog)
    }
}
export default {
    namespaced: true,
    state,
    mutations,
    actions
}