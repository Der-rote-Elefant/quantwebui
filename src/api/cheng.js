import request from '@/utils/request'

export function getTaskList(params) {
  return request({
    url: '/taskList',
    method: 'get',
    baseURL: '/cheng',
    params
  })
}

export function deleteLoopTest(params) {
  return request({
    url: '/deleteTask',
    method: 'get',
    baseURL: '/cheng',
    params
  })
}

export function createTask(params) {
  return request({
    url: '/createTask',
    method: 'post',
    baseURL: '/cheng',
    params
  })
}

export function getLoopResCodes(params) {
  return request({
    url: '/getLoopResCodes',
    method: 'post',
    baseURL: '/cheng',
    params
  })
}

export function getLoopResData(params) {
  return request({
    url: '/loop_res_data',
    method: 'post',
    baseURL: '/cheng',
    params
  })
}

export function getLoopResTable(params) {
  return request({
    url: '/taskLoopResTable',
    method: 'get',
    baseURL: '/cheng',
    params
  })
}

export function loopResProfit(params) {
  return request({
    url: '/loop_res_profit',
    method: 'get',
    baseURL: '/cheng',
    params
  })
}

export function getCodeSearch(params) {
  return request({
    url: '/market/keyboards',
    method: 'get',
    baseURL: '/cheng',
    params
  })
}

export function confirmSearch(params) {
  return request({
    url: '/market/bars2',
    method: 'POST',
    baseURL: '/cheng',
    data: params
  })
}

export function getTreeMap(params) {
  return request({
    url: '/market/treeMap',
    method: 'POST',
    baseURL: '/cheng',
    data: params
  })
}

export function getPeriodType(params) {
  return request({
    url: '/market/get_period_type',
    method: 'POST',
    baseURL: '/cheng',
    data: params
  })
}

export function getCodeData(params) {
  return request({
    url: '/market/bars',
    method: 'post',
    baseURL: '/cheng',
    data: params
  })
}

export function getIndicators(params) {
  return request({
    url: '/market/indicators',
    method: 'get',
    baseURL: '/cheng',
    data: params
  })
}
export function getCulRedisDataList(params) {
  return request({
    url: '/cul/redisDataList',
    method: 'post',
    baseURL: '/cheng',
    data: params
  })
}
export function getCulRedisData(params) {
  return request({
    url: '/cul/redisData',
    method: 'post',
    baseURL: '/cheng',
    data: params
  })
}

export function uploadInfo(data) {
  return request({
    url: '/cul/redisData',
    method: 'post',
    baseURL: '/cheng',
    data: params
  })
}