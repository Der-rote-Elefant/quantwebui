import request from '@/utils/request'

var baseUrl = '/quant_web' 

export function login(data) {
  return request({
    url: '/user/login',
    method: 'post',
    data: data,
    baseURL: baseUrl
  })
}

export function getInfo(token) {
  return request({
    url: '/user/info',
    method: 'get',
    params: { token },
    baseURL: baseUrl
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'post',
    baseURL: baseUrl
  })
}

export function getAllInfo() {
  return request({
    url: '/user/getAllInfo',
    method: 'post',
    baseURL: baseUrl
  })
}

export function getAllUser() {
  return request({
    url: '/user/getAllUser',
    method: 'post',
    baseURL: baseUrl
  })
}
export function getAllRole() {
  return request({
    url: '/user/getAllRole',
    method: 'post',
    baseURL: baseUrl
  })
}
export function saveUser(parm) {
  return request({
    url: '/user/saveUser',
    method: 'post',
    data: parm,
    baseURL: baseUrl
  })
}
export function saveRole(parm) {
  return request({
    url: '/user/saveRole',
    method: 'post',
    data:parm,
    baseURL: baseUrl
  })
}

export function getAllTask(parm) {
  return request({
    url: '/user/getAllTask',
    method: 'GET',
    params:parm,
    baseURL: baseUrl
  })
}

export function saveTask(parm) {
  return request({
    url: '/user/saveTask',
    method: 'post',
    data:parm,
    baseURL: baseUrl
  })
}
export function getSimpleUser(parm) {
  return request({
    url: '/user/getSimpleUser',
    method: 'POST',
    data:parm,
    baseURL: baseUrl
  })
}