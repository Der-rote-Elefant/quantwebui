import request from '@/utils/request'

export function getLoopTest(query) {
    return request({
        url: '/loop_test/get_tag_looptests',
        method: 'get',
        params: query,
        baseURL: '/bf'
    })
}
export function addLoopTest(query) {
    return request({
        url: '/loop_test/add_tag_looptest',
        method: 'post',
        data: query,
        baseURL: '/bf'
    })
}
export function getLoopTestCharts(query) {
    return request({
        url: '/loop_test/charts',
        method: 'post',
        data: query,
        baseURL: '/bf'
    })
}
export function loopTestUpload(query){
    return request({
        url: '/loop_test/upload',
        method: 'post',
        data: query,
        baseURL: '/bf'
    })
}
export function deleteHang(query){
    return request({
        url: '/loop_test/delete',
        method: 'post',
        data: query,
        baseURL: '/bf'
    })
}

export function getDrawdownChart(query){
    return request({
      url: '/loop_test/drawdownChart',
      method: 'post',
      data: query,
      baseURL: '/bf'
    })
}
// export function updateLoopTest(query){
//     return request({
//       url: '/loop_test/update',
//       method: 'post',
//       data: query,
//       baseURL: '/bf'
//     })
// }

export function getPoolList(query){
    return request({
      url: '/looptest/pools',
      method: 'get',
      params: query,
      baseURL: '/bf'
    })
}

export function sendRemoteLooptest(query){
    return request({
      url: '/looptest/rpc_remote',
      method: 'post',
      data: query,
      baseURL: '/bf'
    })
}

export function getStrategyParams(query){
    return request({
      url: '/looptest/strategyParams',
      method: 'post',
      data: query,
      baseURL: '/bf'
    })
}

export function getRemoteRecords(query){
    return request({
      url: '/looptest/records',
      method: 'get',
      params: query,
      baseURL: '/bf'
    })
}

export function removeReportByStrategyName(query){
    return request({
      url: '/looptest/remove_report',
      method: 'get',
      params: query,
      baseURL: '/bf'
    })
}

export function getReportJson(query){
    return request({
      url: '/looptest/report_json',
      method: 'get',
      params: query,
      baseURL: '/bf'
    })
}