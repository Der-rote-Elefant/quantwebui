import request from '@/utils/request'

export function getAllTable(query) {
    return request({
        url: '/loop_test/strategy/allTable',
        method: 'get',
        params: query,
        baseURL: '/bf'
    })
}
export function uploadFile(query) {
    return request({
        url: '/loop_test/strategy/tradeFlow_upload',
        method: 'post',
        data: query,
        baseURL: '/bf'
    })
}
export function updateInfo(query) {
    return request({
        url: '/loop_test/strategy/update',
        method: 'post',
        data: query,
        baseURL: '/bf'
    })
}
export function getChart(query) {
    return request({
        url: '/loop_test/strategy/chart',
        method: 'get',
        params: query,
        baseURL: '/bf'
    })
}
export function getBindFund(query) {
    return request({
        url: '/loop_test/strategy/bindFund',
        method: 'get',
        params: query,
        baseURL: '/bf'
    })
}
export function postBindFund(query) {
    return request({
        url: '/loop_test/strategy/bindFund',
        method: 'post',
        data: query,
        baseURL: '/bf'
    })
}
export function recalculator(query) {
    return request({
        url: '/loop_test/strategy/recalculator',
        method: 'get',
        params: query,
        baseURL: '/bf'
    })
}
export function getTradeDate(query){
    return request({
        url: '/loop_test/strategy/getTradeDate',
        method: 'get',
        params: query,
        baseURL: '/bf'
    })
}
export function getByStrategyId(query){
    return request({
        url: '/loop_test/strategy/getByStrategyId',
        method: 'get',
        params: query,
        baseURL: '/bf'
    })
}
export function saveStrategyFixData(query){
    return request({
        url: '/loop_test/saveStrategyFixData',
        method: 'post',
        data: query,
        baseURL: '/bf'
    })
}
export function getStrategybyIds(query){
    return request({
        url: '/loop_test/getStrategyData',
        method: 'post',
        data: query,
        baseURL: '/bf'
    })
}