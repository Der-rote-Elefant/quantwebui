import request from '@/utils/request'

export function getLog(query) {
    return request({
      url: '/log/get',
      method: 'get',
      params: query,
      baseURL: '/bf'
    })
  }
export function addLog(query) {
    return request({
      url: '/log/add',
      method: 'post',
      data: query,
      baseURL: '/bf'
    })
  }