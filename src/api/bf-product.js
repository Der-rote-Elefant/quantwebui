import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/product/list',
    method: 'get',
    params: query,
    baseURL: '/bf'
  })
}
export function getKafangVwapFile(query){
  return request({
    url: '/product/get_kafang_vwap_file',
    method: 'post',
    data: query,
    baseURL: '/bf',
    responseType:'arraybuffer'
  })
}
export function refreshList(query) {
  return request({
    url: '/product/refresh',
    method: 'get',
    params: query,
    baseURL: '/bf'
  })
}
export function fetchChartData(query) {
  return request({
    url: '/product/newchart',
    method: 'get',
    params: query,
    baseURL: '/bf'
  })
}
export function addLoopTestSolution(query) {
  return request({
    url: '/product/add_loop_test_solution',
    method: 'post',
    data: query,
    baseURL: '/bf'
  })
}
export function getVwapData(query){
  return request({
    url: '/product/vwap',
    method: 'post',
    data: query,
    baseURL: '/bf'
  })
}
export function getKafangVwapData(query){
  return request({
    url: '/product/kafang_vwap',
    method: 'post',
    data: query,
    baseURL: '/bf'
  })
}
export function fetchStrategyData(query) {
  return request({
    url: '/product/strategy',
    method: 'get',
    params: query,
    baseURL: '/bf'
  })
}
export function loopTestUpload(query) {
  return request({
    url: '/product/loop_test_upload',
    method: 'post',
    data: query,
    baseURL: '/bf'
  })
}
export function uploadPB(query) {
  return request({
    url: '/product/pb_upload',
    method: 'post',
    data: query,
    baseURL: '/bf'
  })
}
export function uploadHistory(query) {
  return request({
    url: '/product/history_upload',
    method: 'post',
    data: query,
    baseURL: '/bf'
  })
}
export function uploaTradeFlow(query) {
  return request({
    url: '/product/tradeFlow_upload',
    method: 'post',
    data: query,
    baseURL: '/bf'
  })
}
export function fetchHistoryLoopTest(query) {
  return request({
    url: '/product/historyLoopTest',
    method: 'get',
    params: query,
    baseURL: '/bf'
  })
}
export function fetchPositionData(query) {
  return request({
    url: '/product/position',
    method: 'get',
    params: query,
    baseURL: '/bf'
  })
}
export function addProduct(query) {
  return request({
    url: '/product/add',
    method: 'get',
    params: query,
    baseURL: '/bf'
  })
}
// todo 上传文件功能
export function upinit_fund_by_position_and_profit(query) {
  return request({
    url: '/product/add',
    method: 'get',
    params: query,
    baseURL: '/bf'
  })
}
export function deleteProduct(query) {
  return request({
    url: '/product/delete',
    method: 'post',
    data: query,
    baseURL: '/bf'
  })
}
export function getChartResult(query) {
  return request({
    url: '/product/looptestCharts',
    method: 'post',
    data: query,
    baseURL: '/bf'
  })
}

export function getDrawdownChart(query) {
  return request({
    url: '/product/drawdownChart',
    method: 'post',
    data: query,
    baseURL: '/bf'
  })
}
export function deleteProApi(query) {
  return request({
    url: '/product/deleteProApi',
    method: 'get',
    params: query,
    baseURL: '/bf'
  })
}
export function updatePro(query) {
  return request({
    url: '/product/updatePro',
    method: 'post',
    data: query,
    baseURL: '/bf'
  })
}
export function saveFixData(query){
  return request({
    url: '/product/saveFixData',
    method: 'post',
    data: query,
    baseURL: '/bf'
  })
}
export function getFixTime(query){
  return request({
    url: '/product/getFixTime',
    method: 'get',
    params: query,
    baseURL: '/bf'
  })
}
export function getTableData(query){
  return request({
    url: '/product/getFixTableData',
    method: 'get',
    params: query,
    baseURL: '/bf'
  })
}
export function clearFundInfo(query){
  return request({
    url: '/product/clear_fund_info',
    method: 'get',
    params: query,
    baseURL: '/bf'
  })
}

export function compoundUpload(query) {
  return request({
    url: '/product/upload',
    method: 'post',
    data:query,
    baseURL: '/bf',
    headers:{'Content-Type': 'multipart/form-data'},
  })
}

export function detailUpload(query) {
  return request({
    url: '/product/upload_looptest_flow',
    method: 'post',
    data:query,
    baseURL: '/bf',
    headers:{'Content-Type': 'multipart/form-data'},
  })
}

export function getAccountId(query) {
  return request({
    url: '/product/getAccountId',
    method: 'get',
    params:query,
    baseURL: '/bf',
  })
}

export function getStockDiff(query) {
  return request({
    url: '/product/stockDiff',
    method: 'get',
    params:query,
    baseURL: '/bf',
  })
}

export function getCountToday(query) {
  return request({
    url: '/product/computeToday',
    method: 'get',
    params:query,
    baseURL: '/bf',
  })
}

export function getRealTimeProfit(query) {
  return request({
    url: '/product/realTimeProfit',
    method: 'get',
    params:query,
    baseURL: '/bf',
  })
}

export function getCheckDate(query){
  return request({
    url: '/product/checkDate',
    method: 'get',
    params:query,
    baseURL: '/bf',
  })
}
export function getChartBar(query){
  return request({
    url: '/product/chartBar',
    method: 'get',
    params:query,
    baseURL: '/bf',
  })
}
export function getAccrualData(query){
  return request({
    url: '/product/getAccrualData',
    method: 'get',
    params:query,
    baseURL: '/bf',
  })
}
export function updateAccrualData(query){
  return request({
    url: '/product/saveFixAccrualData',
    method: 'post',
    data:query,
    baseURL: '/bf',
  })
}