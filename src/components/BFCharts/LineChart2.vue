<template>
  <div id="t0_profit_chart" :style="{height:height,width:width}" />
</template>

<script>
import echarts from "echarts";
export default {
  props: {
    width: {
      type: String,
      default: "100%"
    },
    height: {
      type: String,
      default: "68vh"
    },
    chartData: {
      type: Object,
      required: true
    }
  },
  data() {
    return {
      chart: null
    };
  },
  mounted() {
    this.$nextTick(() => {
      this.initChart();
    });
  },
  beforeDestroy() {
    if (!this.chart) {
      return;
    }
    this.chart.dispose();
    this.chart = null;
  },
  methods: {
    initChart() {
      this.chart = echarts.init(document.getElementById("t0_profit_chart"));
      if (Object.keys(this.chartData).length > 0) {
        this.setOptions(this.chartData);
      }
    },
    setOptions(){
        const legendData=Object.keys(this.chartData)
        const seriesData=[]
        for (let k in this.chartData){
            seriesData.push({
                type: "line",
                name: k,
                data:this.chartData[k]
            })
        }
        this.chart.setOption({
        legend: {
          data: legendData
        },
        xAxis: {
          type: "value",
          splitLine: { show: false }
        },
        grid: {
          left: 10,
          right: 20,
          bottom: 25,
          top: 30,
          containLabel: true
        },
        yAxis: {
          type: "value",
          splitLine: { show: false },
        //   min: "dataMin",
        //   max: "dataMax"
        },
        dataZoom: [
          {
            type: "inside"
          },
          {
            type: "slider"
          }
        ],
        tooltip: {
          trigger: "axis",
          axisPointer: {
            type: "cross"
          },
          padding: [5, 10],
          feature: {
            dataZoom: {
              yAxisIndex: false
            }
          }
        },
        series: seriesData
      });
    }
  },
  watch: {
    chartData: {
      deep: true,
      handler(val) {
        this.chart.clear();
        this.setOptions();
        this.chart.resize();
      }
    }
  }
};
</script>

<style>
</style>