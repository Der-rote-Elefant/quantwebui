const Mock = require('mockjs')

const data = Mock.mock({
  'items|30': [{
    id: '@id',
    title: '@sentence(10, 20)',
    'status|1': ['published', 'draft', 'deleted'],
    author: 'name',
    display_time: '@datetime',
    pageviews: '@integer(300, 5000)'
  }]
})

module.exports = [
  {
    url: '/reports/summary',
    type: 'get',
    response: config => {
      return {
        "httpCode": 200,
        "responseEntity": [
          {
            allTrades: [{ "key": "netProfit", "name": "净利润", "value": 81.40 }],
            longTrades: [{ "key": "netProfit", "name": "净利润", "value": 81.50 }],
            shortTrades: [{ "key": "netProfit", "name": "净利润", "value": 81.60 }],
            allSummary: [{ "key": "profitRate", "name": "收益率", "value": "0.00%" }],
            assetWithdraw: [{ "key": "withdraw", "name": "回撤值", "value": 729.6 },],
            assetWithdrawRate: [{ "key": "withdraw", "name": "回撤值", "value": 7.6 },],
          }
        ]
      }
    }
  },
  {
    url: '/tasks/looptest',
    type: 'get',
    response: config => {
      return {
        "httpCode": 200,
        "responseEntity": [{
          applicationId:"app-20201224101404-0040",
          taskName:"回测任务1",
          taskDescription:"破五日均线买入持仓2日",
          taskStatus:"running"
        }
        ]
      }
    }
  },
]
